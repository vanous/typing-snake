# Move fast!

Typing game to help with touch type learning.

Play [here](https://vanous.codeberg.page/typing-game/)

<img src="assets/showcase.gif" alt="screenshot" height=500> 
